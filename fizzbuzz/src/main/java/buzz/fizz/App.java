package buzz.fizz;

/**
 * Hello world!
 *
 */
public class App 
{

   public static String fizzBuzz(int N){
        String ret_val= "";   
        for(int i = 1; i<=N; i++){
            if(i%3==0){
                ret_val+="fizz";
                
            }
            if(i%5==0){
                ret_val+="buzz";
            
            }
            if(i%5!=0 && i%3!=0){
                ret_val+=i;
            }   
                
            ret_val+=" ";
        }
        return ret_val;
    }
}
