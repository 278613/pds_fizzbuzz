package buzz.fizz;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void fizzBuzzTest()
    {
        assertEquals("INPUT: 1","1 ", App.fizzBuzz(1));
        
    }
    @Test
    public void feature_3_Test()
    {
        assertEquals("INPUT: 3","1 2 fizz ", App.fizzBuzz(3));
        
    }
    @Test
    public void feature_5_Test()
    {
        assertEquals("INPUT: 5","1 2 fizz 4 buzz ", App.fizzBuzz(5));
        
    }
    @Test
    public void feature_15_Test()
    {
        assertEquals("INPUT: 5","1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz ", App.fizzBuzz(15));
        
    }
}
